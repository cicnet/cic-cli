# standard imports
import os
import unittest
import logging

# external imports
from hexathon import strip_0x
        
# local imports
from cic.output import KVWriter

# test imports
from tests.base_cic import TestCICBase

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class TestCICOutput(TestCICBase):

    def test_output_file(self):
        self.outputs_writer.write('foo', b'bar')
        fp = os.path.join(self.outputs_dir, 'foo')
        f = open(fp, 'r')
        v = f.read()
        f.close()
        self.assertEqual(v, 'bar')
    

if __name__ == '__main__':
    unittest.main()
