# standard imports
import os
import unittest
import logging

# local imports
from cic import Proof
from cic.attachment import Attachment

# test imports
from tests.base_cic import (
        test_data_dir,
        TestCICBase,
        root_merged_hash,
        )

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

class TestProof(TestCICBase):

    def test_proof_serialize_merge(self):
        proof_path = os.path.join(test_data_dir, 'proof')

        attach = Attachment(proof_path, writer=self.outputs_writer)
        attach.load()

        c = Proof(path=proof_path, attachments=attach, writer=self.outputs_writer)
        c.load()
        v = c.process(token_address=self.token_address, token_symbol=self.token_symbol)
        self.assertEqual(v, root_merged_hash)


if __name__ == '__main__':
    unittest.main()
