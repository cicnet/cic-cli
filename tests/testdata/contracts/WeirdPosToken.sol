contract WeirdPosToken {

	string public name;
	string public symbol;
	uint256 public decimals;
	bytes32 public foo;

	constructor(string memory _symbol, uint8 _decimals, bytes32 _foo, string memory _name) {
		name = _name;
		symbol = _symbol;
		decimals = uint256(_decimals);
		foo = _foo;
	}
}
