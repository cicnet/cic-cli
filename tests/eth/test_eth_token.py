# standard imports
import os
import unittest
import logging

# external imports
from chainlib.eth.tx import (
        TxFormat,
        receipt,
        Tx,
        )
from chainlib.eth.contract import (
        abi_decode_single,
        ABIContractEncoder,
        ABIContractType,
        )
from chainlib.jsonrpc import JSONRPCRequest
from hexathon import add_0x
from giftable_erc20_token import GiftableToken
from eth_erc20 import ERC20

# local imports
from cic.ext.eth import CICEth

# tests imports
from tests.eth.base_eth import TestCICEthTokenBase
from tests.base_cic import test_data_dir

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()
logg.setLevel(logging.DEBUG)


class TestCICEthToken(TestCICEthTokenBase):

    def test_token_nobackend(self):
        self.adapter.prepare_token(self.token_name, self.token_symbol, self.token_precision, GiftableToken.bytecode(), 0)
        self.adapter.token_address = self.token_address
        v = self.adapter.process_token(writer=self.outputs_writer)


    def test_token_sign(self):
        self.adapter.token_address = self.token_address
        self.adapter.signer = self.signer
        self.adapter.tx_format = TxFormat.RLP_SIGNED
        self.adapter.prepare_token(self.token_name, self.token_symbol, self.token_precision, GiftableToken.bytecode(), self.token_supply)
        v = self.adapter.process_token(writer=self.outputs_writer)


    def test_token_rpc(self):
        self.adapter.signer = self.signer
        self.adapter.rpc = self.rpc
        self.adapter.tx_format = TxFormat.JSONRPC
        self.adapter.prepare_token(self.token_name, self.token_symbol, self.token_precision, GiftableToken.bytecode(), self.token_supply)
        v = self.adapter.process_token(writer=self.outputs_writer)
        o = receipt(v)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

    
    def test_token_weirdorder(self):
        weird_contract_bytecode_path = os.path.join(test_data_dir, 'contracts', 'WeirdPosToken.bin')
        f = open(weird_contract_bytecode_path, 'r')
        weird_bytecode = f.read()
        f.close()
        try:
            self.adapter.prepare_token(self.token_name, self.token_symbol, self.token_precision, weird_bytecode, self.token_supply, extra=['0xdeadbeef'], extra_types=['bytes32'])
        except ValueError:
            v = self.adapter.process_token(writer=self.outputs_writer)
        self.adapter.prepare_token(self.token_name, self.token_symbol, self.token_precision, weird_bytecode, self.token_supply, extra=['0xdeadbeef'], extra_types=['bytes32'], positions=[1, 2, 3, 0])
        self.adapter.load_code()
        self.adapter.signer = self.signer
        self.adapter.rpc = self.rpc
        self.adapter.tx_format = TxFormat.JSONRPC
        v = self.adapter.process_token()
        logg.debug('v {}'.format(v))
        o = receipt(v)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)
        Tx.src_normalize(r)
        contract_address = r['contract_address']
        logg.debug('contract address {}'.format(contract_address))

        c = ERC20(self.chain_spec)
        o = c.name(contract_address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        self.assertEqual(c.parse_name(r), self.token_name)

        o = c.symbol(contract_address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        self.assertEqual(c.parse_symbol(r), self.token_symbol)

        o = c.decimals(contract_address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        self.assertEqual(c.parse_decimals(r), self.token_precision)

        j = JSONRPCRequest()
        o = j.template()
        o['method'] = 'eth_call'
        enc = ABIContractEncoder()
        enc.method('foo')
        data = add_0x(enc.get())
        tx = c.template(self.accounts[0], contract_address)
        tx = c.set_code(tx, data)
        o['params'].append(c.normalize(tx))
        o['params'].append('latest')
        o = j.finalize(o)

        r = self.rpc.do(o)
        v = abi_decode_single(ABIContractType.BYTES32, r)
        self.assertEqual(v[-8:], 'deadbeef')


if __name__ == '__main__':
    unittest.main()
