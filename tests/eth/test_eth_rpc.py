# standard imports
import unittest
import logging

# external imports
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.gas import RPCGasOracle
from chainlib.eth.tx import (
        TxFormat,
        unpack,
        )
from hexathon import strip_0x
        
# local imports
from cic.ext.eth import CICEth

# test imports
from tests.eth.base_eth import TestCICEthBase

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class TestCICEthRPC(TestCICEthBase):

    def setUp(self):
        super(TestCICEthRPC, self).setUp()
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        gas_oracle = RPCGasOracle(self.rpc)
        self.adapter = CICEth(self.core_processor, self.chain_spec, self.resources, self.proofs, signer=self.signer, rpc=self.rpc, fee_oracle=gas_oracle)


    @unittest.skip('test does not make sense anymore because nonces will be different')
    def test_rpc_token_index(self):
        self.adapter.token_address = self.token_address
        v = self.adapter.process_token_index()

        adapter_norpc = CICEth(self.core_processor, self.chain_spec, self.resources, self.proofs, signer=self.signer)
        adapter_norpc.token_address = self.token_address
        vv = adapter_norpc.process_token_index()

        tx_raw = bytes.fromhex(strip_0x(vv))
        tx = unpack(tx_raw, self.chain_spec)
        self.assertEqual(strip_0x(tx['hash']), strip_0x(v))


if __name__ == '__main__':
    unittest.main()
