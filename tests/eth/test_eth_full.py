# standard imports
import unittest
import logging
import random

# external imports
from chainlib.eth.nonce import (
        RPCNonceOracle,
        nonce,
        )
from chainlib.eth.gas import (
        RPCGasOracle,
        OverrideGasOracle,
        )
from chainlib.eth.tx import (
        TxFormat,
        unpack,
        receipt,
        Tx,
        )
from hexathon import (
        strip_0x,
        add_0x,
        )
from eth_token_index import TokenUniqueSymbolIndex
from giftable_erc20_token import GiftableToken
        
# local imports
from cic.ext.eth import CICEth
from cic.processor import Processor

# test imports
from tests.eth.base_eth import TestCICEthTokenBase

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()
logg.setLevel(level=logging.DEBUG)


class TestCICEthRPC(TestCICEthTokenBase):

    def setUp(self):
        super(TestCICEthRPC, self).setUp()
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        gas_oracle = RPCGasOracle(self.rpc)

        self.adapter = CICEth(self.chain_spec, self.resources, self.proofs, signer=self.signer, rpc=self.rpc, fee_oracle=gas_oracle, outputs_writer=self.outputs_writer)
        self.core_processor = Processor(outputs_writer=self.outputs_writer, extensions=[self.adapter])


    def test_rpc_process_notoken(self):
        addresses = self.keystore.list()
        nonce_oracle = RPCNonceOracle(addresses[0], self.rpc)
        gas_oracle = OverrideGasOracle(limit=GiftableToken.gas(), conn=self.rpc)
        c = GiftableToken(self.chain_spec, self.signer, nonce_oracle=nonce_oracle, gas_oracle=gas_oracle)
        (tx_hash_hex, o) = c.constructor(addresses[0], self.token_name, self.token_symbol, self.token_precision)
        r = self.rpc.do(o)
        o = receipt(r)
        r = self.rpc.do(o)
        Tx.src_normalize(r)
        self.assertEqual(r['status'], 1)
        token_address = r['contract_address']
        self.adapter.resources['token']['reference'] = token_address

        self.token_index_address = r['contract_address']
        logg.debug('token index deployed at {}'.format(self.token_index_address))

        self.adapter.process()
        results = self.adapter.get_outputs()
        for v in results:
            o = receipt(v[1])
            r = self.rpc.do(o)
            self.assertEqual(r['status'], 1)


    def test_rpc_process_withtoken(self):
        self.adapter.fee_oracle = OverrideGasOracle(limit=GiftableToken.gas(), conn=self.rpc)
        self.adapter.prepare_token(self.token_name, self.token_symbol, self.token_precision, GiftableToken.bytecode(), self.token_supply)
        self.adapter.process()
        
        results = self.adapter.get_outputs()
        for v in results:
            o = receipt(v[1])
            r = self.rpc.do(o)
            self.assertEqual(r['status'], 1)

        o = receipt(results[0][1])
        r = self.rpc.do(o)
        token_contract_address = r['contract_address']

        c = TokenUniqueSymbolIndex(self.chain_spec)
        o = c.address_of(self.token_index_address, 'FOO', sender_address=self.accounts[0])
        r = self.rpc.do(o)
        token_contract_address_lookup = c.parse_address_of(r)
        self.assertEqual(strip_0x(token_contract_address), strip_0x(token_contract_address_lookup))


    def test_rpc_process_top(self):
        self.adapter.fee_oracle = OverrideGasOracle(limit=GiftableToken.gas(), conn=self.rpc)
        self.adapter.prepare_token(self.token_name, self.token_symbol, self.token_precision, GiftableToken.bytecode(), self.token_supply)
        self.core_processor.process()

        results = self.adapter.get_outputs()
        for v in results:
            logg.debug('results {}'.format(v))
            o = receipt(v[1])
            r = self.rpc.do(o)
            self.assertEqual(r['status'], 1)

        o = receipt(results[0][1])
        r = self.rpc.do(o)
        token_contract_address = r['contract_address']

        c = TokenUniqueSymbolIndex(self.chain_spec)
        o = c.address_of(self.token_index_address, 'FOO', sender_address=self.accounts[0])
        r = self.rpc.do(o)
        token_contract_address_lookup = c.parse_address_of(r)
        self.assertEqual(strip_0x(token_contract_address), strip_0x(token_contract_address_lookup))


if __name__ == '__main__':
    unittest.main()
