# standard imports
import unittest
import logging

# external imports
from chainlib.eth.tx import unpack
from hexathon import (
        strip_0x,
        add_0x,
        )

# local imports
from cic.ext.eth import CICEth
from cic.processor import Processor

# tests imports
from tests.eth.base_eth import TestCICEthBase

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class TestCICEthSign(TestCICEthBase):

    def setUp(self):
        super(TestCICEthSign, self).setUp()
        self.adapter = CICEth(self.chain_spec, self.resources, self.proofs, signer=self.signer)
        self.core_processor = Processor(outputs_writer=self.outputs_writer, extensions=[self.adapter])


    def test_sign_token_index(self):
        self.adapter.token_address = self.token_address
        v = self.adapter.process_token_index(writer=self.outputs_writer)
        tx_raw = bytes.fromhex(strip_0x(self.adapter.outputs[0][1])) 
        tx = unpack(tx_raw, self.chain_spec)
        self.assertEqual(strip_0x(tx['data'])[:8], '4420e486')


if __name__ == '__main__':
    unittest.main()
