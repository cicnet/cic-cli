# standard imports
import unittest
import logging
import os

# local imports
from cic.ext.eth import CICEth
from cic.processor import Processor

# tests imports
from tests.eth.base_eth import TestCICEthBase

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class TestCICEthOffline(TestCICEthBase):

    def setUp(self):
        super(TestCICEthOffline, self).setUp()
        self.adapter = CICEth(self.chain_spec, self.resources, self.proofs)
        proofs = []
        for proof in self.proofs.get():
            proofs.append(proof[0])
        proofs.sort()
        self.first_proof = proofs[0]
        #self.core_processor = Processor(outputs_writer=self.outputs_writer, extensions=[self.adapter])


    def test_offline_token_index(self):
        self.adapter.token_address = self.token_address
        self.adapter.process_token_index(writer=self.outputs_writer)
        self.assertEqual(self.adapter.outputs[0][0], 'token_index')
        self.assertEqual(self.adapter.outputs[0][1][:8], '4420e486')


    def test_offline_address_declarator(self):
        self.adapter.token_address = self.token_address
        self.adapter.process_address_declarator()
        first_proof = self.proofs.get()[0]
        self.assertEqual(self.adapter.outputs[0][0], 'address_declarator_' + self.first_proof)
        self.assertEqual(self.adapter.outputs[0][1][:8], 'ae47ece0')
        self.assertEqual(len(self.adapter.outputs),  3)


    def test_offline_writer(self):
        self.adapter.outputs_writer = self.outputs_writer
        self.adapter.token_address = self.token_address
        self.adapter.process_address_declarator()
        logg.debug('proocs {}'.format(self.proofs))
        logg.debug('outputs {}'.format(self.adapter.outputs))
        self.assertEqual(self.adapter.outputs[0][0], 'address_declarator_' + self.first_proof)
        self.assertEqual(self.adapter.outputs[0][1][:8], 'ae47ece0')
        self.assertEqual(len(self.adapter.outputs),  3)

        proofs = self.proofs.get()
        for i, v in enumerate(self.adapter.outputs):
            fp = os.path.join(self.outputs_dir, v[0])
            f = open(fp, 'rb')
            r = f.read()
            f.close()
            self.assertEqual(r.decode('utf-8'), v[1])


if __name__ == '__main__':
    unittest.main()
