# standard imports
import os
import tempfile
import logging
import unittest
import random

# external imports
from hexathon import add_0x

# local imports
from cic.output import KVWriter
from cic.processor import Processor
from cic.attachment import Attachment
from cic import Proof

logg = logging.getLogger(__name__)

test_base_dir = os.path.dirname(os.path.realpath(__file__))
test_data_dir = os.path.join(test_base_dir, 'testdata')

proof_hash = '0f6fc017f29caf512c0feaaf83bc10614b488311cace2973dc248dc24b01e04f'
foo_hash = '2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae'
bar_hash = 'fcde2b2edba56bf408601fb721fe9b5c338d10ee429ea04fae5511b68fbf8fb9'
root_merged_hash = '4bd0ad4305a5fee20fb80e179a437c296f6a769ca376d746a3848a80e9b7a1a6'


class TestCICBase(unittest.TestCase):

    def setUp(self):
        super(TestCICBase, self).setUp()
        random.seed(42)

        addresses = [
            add_0x(random.randbytes(20).hex()),
            add_0x(random.randbytes(20).hex()),
            add_0x(random.randbytes(20).hex()),
                ]
        self.token_symbol = 'FOO'
        self.token_address = add_0x(random.randbytes(32).hex())
        self.token_index_address = add_0x(random.randbytes(32).hex())
        self.address_declarator_address = add_0x(random.randbytes(32).hex())

        self.outputs_dir = tempfile.mkdtemp()
        self.outputs_writer = KVWriter(self.outputs_dir)
        self.core_processor = Processor(outputs_writer=self.outputs_writer)

        self.resources = {
                'token': {
                    'reference': self.token_address,
                    'key_address': addresses[0],
                    },
                'token_index': {
                    'reference': self.token_index_address,
                    'key_address': addresses[1],
                    },
                'address_declarator': {
                    'reference': self.address_declarator_address,
                    'key_address': addresses[2],
                    },
            }
        proof_dir = os.path.join(test_data_dir, 'proof')
        attach = Attachment(path=proof_dir)
        attach.load()
        self.proofs = Proof(proof_dir, attachments=attach)
        self.proofs.load()
