# standard imports
import unittest
import logging
import os

# external imports
from hexathon import strip_0x
        
# local imports
from cic.meta import Meta

# test imports
from tests.base_cic import (
        TestCICBase,
        test_data_dir,
        )

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class TestCICMeta(TestCICBase):

    def test_meta(self):
        fp = os.path.join(test_data_dir, 'proof')
        m = Meta(fp)
        m.load()


if __name__ == '__main__':
    unittest.main()
