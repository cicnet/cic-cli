# standard imports
import os
import unittest
import logging

# local imports
from cic.keystore import KeystoreDirectory
from funga.eth.keystore.dict import DictKeystore
from funga.error import DecryptError
from hexathon import uniform as hex_uniform

# test imports
from tests.base_cic import test_base_dir

logging = logging.getLogger()

script_dir = test_base_dir

def pass_getter():
    return 'test'


class EthKeystoreDirectory(DictKeystore, KeystoreDirectory):
    pass


class TestKeyfile(unittest.TestCase):

    def setUp(self):
        self.path = os.path.join(script_dir, 'testdata', 'keystore')
        self.keystore = EthKeystoreDirectory()


    def test_keystore_bogus(self):
        bogus_path = os.path.join(self.path, 'bogus')
        self.keystore.process_dir(bogus_path)


    def test_keystore_ok(self):
        ok_path = os.path.join(self.path, 'ok')
        with self.assertRaises(DecryptError):
            self.keystore.process_dir(ok_path) # wrong password
        self.keystore.process_dir(ok_path, default_password='test')
        self.keystore.process_dir(ok_path, password_retriever=pass_getter)
        self.assertTrue(hex_uniform('cc4f82F5DacDE395E1E0CFc4d62827C8B8B5688C') in self.keystore.list())


if __name__ == '__main__':
    unittest.main()
