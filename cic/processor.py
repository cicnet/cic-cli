# standard imports
import logging

logg = logging.getLogger(__name__)


class Processor:
    
    def __init__(self, proof=None, attachment=None, metadata=None, outputs_writer=None, extensions=[]):
        self.token_address = None
        self.extensions = extensions
        self.cores = {
            'metadata': metadata,
            'attachment': attachment,
            'proof': proof,
                }
        self.outputs = []
        self.__outputs_writer = outputs_writer


    def writer(self):
        return self.__outputs_writer


    def get_outputs(self):
        outputs = []
        for ext in self.extensions:
            outputs += ext.outputs
        outputs += self.outputs
        return outputs


    def process(self, writer=None):

        tasks = [
            'attachment',
            'proof',
            'metadata',
            ]

        for ext in self.extensions:
            (token_address, token_symbol) = ext.process()

            for task in tasks:
                a = self.cores.get(task)
                if a == None:
                    logg.debug('skipping missing task receiver "{}"'.format(task))
                    continue
                v = a.process(token_address=token_address, token_symbol=token_symbol, writer=self.__outputs_writer)
                self.outputs.append(v)
