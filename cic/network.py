# standard imports
import os
import json
import logging

# external imports
from chainlib.chain import ChainSpec

# local imports
from .base import (
        Data,
        data_dir,
        )

logg = logging.getLogger(__name__)


class Network(Data):

    def __init__(self, path='.', targets=[]):
        super(Network, self).__init__()
        self.resources = None
        self.path = path
        self.targets = targets
        self.network_path = os.path.join(self.path, 'network.json')


    def load(self):
        super(Network, self).load()

        f = open(self.network_path, 'r')
        o = json.load(f)
        f.close()

        self.resources = o['resources']

        self.inited = True


    def start(self):
        super(Network, self).load()

        network_template_file_path = os.path.join(data_dir, 'network_template_v{}.json'.format(self.version()))
        
        f = open(network_template_file_path)
        o_part = json.load(f)
        f.close() 

        self.resources = {}
        for v in self.targets:
            self.resources[v] = o_part

        self.save()


    def save(self):
        f = open(self.network_path, 'w')
        json.dump({
            'resources': self.resources,
            }, f, sort_keys=True, indent="\t")
        f.close()


    def resource(self, k):
        v = self.resources.get(k)
        if v  == None:
            raise AttributeError('no defined reference for {}'.format(k))
        return v


    def resource_set(self, resource_key, content_key, reference, key_account=None):
        self.resources[resource_key]['contents'][content_key]['reference'] = reference
        self.resources[resource_key]['contents'][content_key]['key_account'] = key_account


    def chain_spec(self, k):
        v = self.resource(k)
        return ChainSpec.from_dict(v['chain_spec'])


    def set(self, resource_key, chain_spec):
        chain_spec_dict = chain_spec.asdict()
        for k in chain_spec_dict.keys():
            logg.debug('resources {}'.format(self.resources))
            self.resources[resource_key]['chain_spec'][k] = chain_spec_dict[k]


    def __str__(self):
        s = ''
        for k in self.resources.keys():
            for kk in self.resources[k]['contents'].keys():
                v = self.resources[k]['contents'][kk]
                if v == None:
                    v = ''
                s += '{}.{} = {}\n'.format(k, kk, v)

        return s
