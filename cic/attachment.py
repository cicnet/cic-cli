# standard imports
import os
import logging

# local imports
from .base import *

logg = logging.getLogger(__name__)


class Attachment(Data):

    def __init__(self, path='.', writer=None):
        super(Attachment, self).__init__()
        self.contents = {}
        self.path = path
        self.writer = writer
        self.attachment_path = os.path.join(self.path, 'attachments')


    def load(self):
        for s in os.listdir(self.attachment_path):
            fp = os.path.realpath(os.path.join(self.attachment_path, s))
            f = open(fp, 'rb')
            r = f.read()
            f.close()

            z = self.hash(r).hex()
            self.contents[z] = fp

            logg.debug('loaded attachment file {} digest {}'.format(fp, z))


    def start(self):
        super(Attachment, self).start()
        os.makedirs(self.attachment_path)


    def get(self, k):
        return self.contents[k]


    def asdict(self):
        return self.contents


    def process(self, token_address=None, token_symbol=None, writer=None):
        if writer == None:
            writer = self.writer

        for k in self.contents.keys():
            fp = os.path.join(self.attachment_path, self.contents[k])
            f = open(fp, 'rb')
            v = f.read()
            f.close()
            logg.debug('writing attachment {}'.format(k))
            writer.write(k, v)


    def __str__(self):
        s = ''
        for k in self.contents.keys():
            s += '{} = {}\n'.format(k, self.contents[k]) #self.digests[i].hex(), self.contents[i])

        return s
