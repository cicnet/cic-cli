# standard imports
import os
import json

# local imports
from .base import (
        Data,
        data_dir,
        )


class Token(Data):

    def __init__(self, path='.', name=None, symbol=None, precision=1, supply=0, code=None):
        super(Token, self).__init__()
        self.name = name
        self.symbol = symbol
        self.supply = supply
        self.precision = precision
        self.code = code
        self.extra_args = None
        self.path = path
        self.token_path = os.path.join(self.path, 'token.json')


    def load(self):
        super(Token, self).load()

        f = open(self.token_path, 'r')
        o = json.load(f)
        f.close()

        self.name = o['name']
        self.symbol = o['symbol']
        self.precision = o['precision']
        self.code = o['code']
        self.supply = o['supply']
        self.extra_args = o['extra']

        self.inited = True


    def start(self):
        super(Token, self).load()

        token_template_file_path = os.path.join(data_dir, 'token_template_v{}.json'.format(self.version()))
        
        f = open(token_template_file_path)
        o = json.load(f)
        f.close() 

        o['name'] = self.name
        o['symbol'] = self.symbol
        o['precision'] = self.precision
        o['code'] = self.code
        o['supply'] = self.supply

        f = open(self.token_path, 'w')
        json.dump(o, f, sort_keys=True, indent="\t")
        f.close()


    def __str__(self):
        s = """name = {}
symbol = {}
precision = {}
""".format(self.name, self.symbol, self.precision)
        return s
