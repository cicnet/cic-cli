class Hasher:

    def __basehasher(self, v):
        h = hashlib.sha256()
        h.update(v)
        return h.digest()


    def hash(self, v):
        return self.__basehasher(v)
