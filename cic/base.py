# standard imports
import os
import hashlib


mod_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.join(mod_dir, '..')
data_dir = os.path.join(mod_dir, 'data')
schema_dir = os.path.join(mod_dir, 'schema')


class Data:


    __default_version = 0

    def __init__(self):
        self.dirty = False
        self.inited = False
        self.__version = self.__default_version
        self.__hasher = self.__basehasher


    def __basehasher(self, v):
        h = hashlib.sha256()
        h.update(v)
        return h.digest()


    def hash(self, v):
        return self.__hasher(v)


    def load(self):
        if self.dirty:
            raise RuntimeError('Object contains uncommitted changes')


    def start(self):
        if self.inited:
            raise RuntimeError('Object already initialized')


    def verify(self):
        return True


    def version(self):
        return self.__version


    def set_version(self, version):
        self.__version = version

