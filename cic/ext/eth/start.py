# external imports
from cic_eth_registry import CICRegistry
from chainlib.eth.connection import RPCConnection


def extension_start(network, *args, **kwargs):
    CICRegistry.address = kwargs['registry_address']

    RPCConnection.register_location(kwargs['rpc_provider'], kwargs['chain_spec'])
    conn = RPCConnection.connect(kwargs['chain_spec'])

    registry = CICRegistry(kwargs['chain_spec'], conn)

    address_declarator = registry.by_name('AddressDeclarator')
    network.resource_set('eth', 'address_declarator', address_declarator)

    token_index = registry.by_name('TokenRegistry')
    network.resource_set('eth', 'token_index', token_index)

    network.set('eth', kwargs['chain_spec'])
    network.save()
