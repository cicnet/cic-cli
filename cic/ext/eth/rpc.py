# standard imports
import stat
import os
import logging

# external imports
from funga.eth.keystore.dict import DictKeystore
from funga.eth.signer import EIP155Signer
from chainlib.eth.cli import Rpc
from chainlib.cli import Wallet

# local imports
from cic.keystore import KeystoreDirectory

logg = logging.getLogger(__name__)


class EthKeystoreDirectory(DictKeystore, KeystoreDirectory):
    pass


def parse_adapter(config, signer_hint):

    keystore = None
    if signer_hint == None:
        logg.info('signer hint missing')
        return None
    st = os.stat(signer_hint)
    if stat.S_ISDIR(st.st_mode):
        logg.debug('signer hint is directory')
        keystore = EthKeystoreDirectory()
        keystore.process_dir(signer_hint)

    w = Wallet(EIP155Signer, keystore=keystore)
    signer = EIP155Signer(keystore)
    rpc = Rpc(wallet=w)
    rpc.connect_by_config(config)

    return (rpc.conn, signer)
