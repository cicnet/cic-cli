# standard imports
import logging
import os

# local imports
from cic import Proof
from cic.meta import Meta
from cic.attachment import Attachment
from cic.network import Network
from cic.token import Token

logg = logging.getLogger(__name__)


def process_args(argparser):
    argparser.add_argument('--target', action='append', type=str, default=[], help='initialize network specification file with target')
    argparser.add_argument('--name', type=str, help='token name')
    argparser.add_argument('--symbol', type=str, help='token symbol')
    argparser.add_argument('--precision', type=str, help='token unit precision')
    argparser.add_argument('directory', help='directory to initialize')


def validate_args(args):
    pass


def execute(config, eargs):
    os.makedirs(eargs.directory)

    ct = Token(eargs.directory, name=eargs.name, symbol=eargs.symbol, precision=eargs.precision)
    cp = Proof(eargs.directory)
    cm = Meta(eargs.directory)
    ca = Attachment(eargs.directory)
    cn = Network(eargs.directory, targets=eargs.target)

    ct.start()
    cp.start()
    cm.start()
    ca.start()
    cn.start()
