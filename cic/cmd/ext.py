# standard imports
import importlib

# external imports
from chainlib.chain import ChainSpec
# local imports
from cic.network import Network


def process_args(argparser):
    argparser.add_argument('--registry', required=True, type=str, help='contract registry address')
    argparser.add_argument('-d', '--directory', type=str, dest='directory', default='.', help='directory')
    argparser.add_argument('-p', type=str, help='RPC endpoint')
    argparser.add_argument('-i', type=str, help='chain spec string')
    argparser.add_argument('target', help='target to initialize')


def validate_args(args):
    pass


def execute(config, eargs):
    cn = Network(eargs.directory, targets=eargs.target)
    cn.load()


    chain_spec = ChainSpec.from_chain_str(eargs.i)
    m = importlib.import_module('cic.ext.{}.start'.format(eargs.target))
    m.extension_start(cn, registry_address=eargs.registry, chain_spec=chain_spec, rpc_provider=config.get('RPC_PROVIDER'))

